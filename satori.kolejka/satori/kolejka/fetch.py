#!/usr/bin/env python2
# -*- coding: utf-8 -*-
# vim:ts=4:sts=4:sw=4:expandtab

KOLEJKA_JUDGE_LIBRARY = 'kolejka-judge'

from satori.client.common import want_import

import base64
import glob
import hashlib
import os
import resource
import stat
import sys
import shutil
import tempfile
import time
import shutil
import subprocess
import traceback
import yaml

from satori.kolejka import *

from satori.kolejka.util import ctxyaml, blob

options.add_argument('--retry-time', dest='retry_time', default=5, type=int)

class Dumper(yaml.SafeDumper):
    pass
class Filename:
    def __init__(self, path):
        self.path = path
def filename_dump(dumper, val):
    return dumper.represent_scalar('!file', '%s'%(val.path))
Dumper.add_representer(Filename, filename_dump)

def satori_fetch(test_result, test_data, submit_data):
    res = dict()
    res['status'] = {'is_blob':False, 'value':'INT'}
    res['description'] = {'is_blob':False, 'value':'Download of checking specs failed'}
    
    temp_path = os.path.join( option_value('work_dir'), 'temp' )
    try:
        os.makedirs(temp_path)
    except os.error:
        pass

    jail_path = tempfile.mkdtemp(dir=temp_path)
    try:
        judge_path = os.path.join(jail_path, 'judge.py')
        library_path = os.path.join(jail_path, KOLEJKA_JUDGE_LIBRARY)
        tests_path = os.path.join(jail_path, 'tests.yaml')
        res_path = os.path.join(jail_path, 'tests')
        task_path = os.path.join(res_path, 'task')
        satoriid_path = os.path.join(res_path, 'satori.id')
        solution_path = None

        os.makedirs(res_path)
        with open(satoriid_path, 'w') as sid:
            sid.write(str(test_result))

        if submit_data.get('content') and submit_data.get('content').is_blob:
            os.makedirs(os.path.join(jail_path, 'solution'))
            solution_path = os.path.join(jail_path, 'solution', os.path.basename(submit_data.get('content').filename))
            submit_data.get_blob_path('content', solution_path)
        else:
            res['description'] = {'is_blob':False, 'value':'No solution'}
            return res

        if test_data.get('judge') and test_data.get('judge').is_blob:
            test_data.get_blob_path('judge', judge_path)
        else:
            res['description'] = {'is_blob':False, 'value':'No judge specified'}
            return res

        if test_data.get(KOLEJKA_JUDGE_LIBRARY) and test_data.get(KOLEJKA_JUDGE_LIBRARY).is_blob:
            test_data.get_blob_path(KOLEJKA_JUDGE_LIBRARY, library_path)
        else:
            subprocess.check_call(['kolejka-judge', 'update', judge_path])

        try:
            kolejka = yaml.safe_load(test_data.get('kolejka').value)
        except:
            res['description'] = {'is_blob':False, 'value':'Wrong kolejka/satori settings'}
            return res

        description = dict()
        description['kolejka'] = kolejka

        try:
            for key,val in test_data.get_map().items():
                if key in ('judge', KOLEJKA_JUDGE_LIBRARY, 'kolejka',):
                    continue
                if val.is_blob:
                    os.makedirs(os.path.join(jail_path, 'test', key))
                    ref_path = os.path.join('test', key, os.path.basename(val.filename))
                    blob_path = os.path.join(jail_path, ref_path)
                    test_data.get_blob_path(key, blob_path)
                    description[key] = Filename(ref_path)
                else:
                    description[key] = str(val.value)

            if 'name' not in description:
                description['name'] = 'test'

            with open(tests_path, 'wb') as tests_file:
                yaml.dump({ description['name']  : description}, tests_file, Dumper=Dumper)

            subprocess.check_call(['kolejka-judge', 'task', judge_path, tests_path, solution_path, task_path])
            for root, dirs, files in os.walk(task_path):
                for f in files:
                    ff = os.path.join(root, f)
                    if os.path.islink(ff):
                        rp = os.path.realpath(ff)
                        os.unlink(ff)
                        shutil.copy(rp, ff)
        except:
            res['description'] = {'is_blob':False, 'value':'Prepare of kolejka task failed'}
            return res
            
        dest_path = conf_tests_path(test_result)
        try:
            os.makedirs(os.path.dirname(dest_path))
        except os.error:
            pass
        try:
            if os.path.exists(dest_path):
                shutil.rmtree(dest_path)
        except os.error:
            pass
        shutil.move(res_path, dest_path)
        return None

    except:
        return res

    finally:
        shutil.rmtree(jail_path)

def satori_fetch_loop():
    while True:
        authenticate()
        submit = Judge.kolejka_get_next()
        if submit != None:
            res = dict()
            res['status'] = {'is_blob':False, 'value':'INT'}
            res['description'] = {'is_blob':False, 'value':'Download of checking specs failed'}
            test_result = submit['test_result']
            try:
                test_data = OaMap(submit['test_data'])
                submit_data = OaMap(submit['submit_data'])
                res = satori_fetch(test_result, test_data, submit_data)
            finally:
                if res is not None:
                    Judge.set_result(test_result, res)
        else:
            time.sleep(option_value('retry_time'))

def main():
    want_import(globals(), '*')
    setup()
    while True:
        try:
            satori_fetch_loop()
        except KeyboardInterrupt:
            break
        except:
            traceback.print_exc()
        time.sleep(10)

if __name__ == '__main__':
    main()
