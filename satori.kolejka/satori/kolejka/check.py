#!/usr/bin/env python2
# -*- coding: utf-8 -*-
# vim:ts=4:sts=4:sw=4:expandtab

from satori.client.common import want_import

from multiprocessing import Process, Pool

import base64
import glob
import hashlib
import os
import resource
import stat
import sys
import shutil
import tempfile
import time
import shutil
import subprocess
import traceback
import yaml

from satori.kolejka import *

from satori.kolejka.util import ctxyaml, blob
from satori.kolejka.util.upload import upload_blob

options.add_argument('--concurrency', default=8, type=int)

def kolejka_send(test_result):
    try:
        task_path = conf_task_path(test_result)
        with open(os.devnull, 'wb') as DEVNULL:
            task_id = subprocess.check_output(['kolejka-client', 'task', 'put', task_path], stderr=DEVNULL).strip()
            #TODO: If real problem, then remove and notify satori
        if task_id:
            with open(conf_kolejkaid_path(test_result), 'w') as kid:
                kid.write(task_id)
    except KeyboardInterrupt:
        raise
    except:
        traceback.print_exc()
        print 'Failed to send %d to kolejka'%(test_result,)
        pass

def kolejka_fetch(test_result):
    try:
        kolejka_id = None
        with open(conf_kolejkaid_path(test_result)) as kid:
            kolejka_id = str(kid.read(1024)).strip()
        if kolejka_id:
            result_path = conf_result_path(test_result)
            temp_path = os.path.join( option_value('work_dir'), 'temp' )
            try:
                os.makedirs(temp_path)
            except os.error:
                pass
            jail_path = tempfile.mkdtemp(dir=temp_path)
            try:
                with open(os.devnull, 'wb') as DEVNULL:
                    subprocess.check_call(['kolejka-client', 'result', 'get', kolejka_id, jail_path], stdout=DEVNULL, stderr=subprocess.STDOUT)
                    results_yaml_path = os.path.join(jail_path, 'results', 'results.yaml')
                    results_json_path = os.path.join(jail_path, 'kolejka_result.json')
                    if os.path.exists(results_yaml_path) or os.path.exists(results_json_path):
                        shutil.move(jail_path, result_path)
                    #TODO: If task does not exist, then kolejka_send
                    #TODO: If real problem, then remove and notify satori
            finally:
                if os.path.exists(jail_path):
                    shutil.rmtree(jail_path)
    except KeyboardInterrupt:
        raise
    except:
        print 'Failed to receive %d from kolejka'%(test_result,)
        pass

def satori_send(test_result):
    try:
        result_path = conf_result_path(test_result)
        results_yaml_path = os.path.join(result_path, 'results', 'results.yaml')
        results_json_path = os.path.join(result_path, 'kolejka_result.json')
        args = ['satori.kolejka.submit']
        if os.path.exists(results_yaml_path):
            args += ['--results', results_yaml_path]
        if os.path.exists(results_json_path):
            args += ['--kolejka-result', results_json_path]
        for param,value in [
                ('-c', 'config'),
                ('-s', 'section'),
                ('-H', 'host'),
                ('-u', 'username'),
                ('-p', 'password'),
                ('-m', 'machine'),
            ]:
            if option_value(value) is not None:
                args += [param, option_value(value)]
        if option_value('ssl'):
            args += [ '-S' ]
        args += [str(test_result)]
        with open(os.devnull, 'wb') as DEVNULL:
            print args
            subprocess.check_call(args)#, stdout=DEVNULL, stderr=subprocess.STDOUT)
    except KeyboardInterrupt:
        raise
    except:
        traceback.print_exc()
        print 'Failed to send %d to satori'%(test_result,)
        pass
    return True

def handle_dir(d):
    try:
        if not os.path.isdir(d):
            return

        satori_id = None
        try:
            with open(os.path.join(d,'satori.id')) as sid:
                satori_id = int(sid.read(1024).strip())
        except:
            pass
        if not satori_id:
            print('Directory %s does not contain satori id. Removing.' % (d,) )
            shutil.rmtree(d)
            return
        if os.path.join(d,'satori.id') != conf_satoriid_path(satori_id):
            print('Directory %s does not match the current dir spec for %d. Removing.' % (d,satori_id) )
            shutil.rmtree(d)
            return
        if not os.path.isdir(conf_task_path(satori_id)):
            print('Directory %s does not contain task. Removing.' % (d,) )
            shutil.rmtree(d)
            return

        kolejka_id = None
        try:
            with open(conf_kolejkaid_path(satori_id)) as kid:
                kolejka_id = str(kid.read(1024)).strip()
        except:
            pass
        if not kolejka_id:
            print('Directory %s does not contain kolejka id. Sending to kolejka.' % (d,) )
            kolejka_send(satori_id)
            return

        if not os.path.isdir(conf_result_path(satori_id)):
            kolejka_fetch(satori_id)

        if os.path.isdir(conf_result_path(satori_id)):
            if satori_send(satori_id):
                print('Directory %s sent to satori. Removing.' % (d,) )
                if os.path.exists(conf_done_path(satori_id)):
                    shutil.rmtree(conf_done_path(satori_id))
                shutil.move(d, conf_done_path(satori_id))
    except KeyboardInterrupt:
        raise
    except:
        traceback.print_exc()


def kolejka_fetch_loop():
    pool = Pool(option_value('concurrency'))
    while True:
        dirs = []
        for path in glob.glob(os.path.join( option_value('work_dir'), 'tests', '*' )):
            if os.path.isdir(path):
                dirs.append(path)
        pool.map(handle_dir, dirs)
        time.sleep(1)

def main():
    setup()
    while True:
        try:
            kolejka_fetch_loop()
        except KeyboardInterrupt:
            break
        except:
            traceback.print_exc()
        time.sleep(10)

if __name__ == '__main__':
    main()
