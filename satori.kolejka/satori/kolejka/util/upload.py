# -*- coding: utf-8 -*-
# vim:ts=4:sts=4:sw=4:expandtab

import base64
import hashlib
import os.path
import sys
from satori.kolejka.util import blob

from satori.client.common import want_import

want_import(globals(), '*')

def copy_file(src, dst):
    BUF_SIZ = 16 * 1024
    while True:
        buf = src.read(BUF_SIZ)
        if not buf:
            break
        dst.write(buf)

def _calculate_blob_hash(blob_path):
    with open(blob_path, 'rb') as blob:
        return base64.urlsafe_b64encode(hashlib.sha384(blob.read()).digest())

def upload_blob(blob_path):
    blob_hash = _calculate_blob_hash(blob_path)
    if not Blob.exists(blob_hash):
        with open(blob_path, 'rb') as local_blob:
            blob_size = os.path.getsize(blob_path)
            remote_blob = Blob.create(blob_size)
            copy_file(local_blob, remote_blob)
        remote_blob_hash = remote_blob.close()
        assert blob_hash == remote_blob_hash
    blob_name = os.path.basename(blob_path)
    return AnonymousAttribute(is_blob=True, value=blob_hash, filename=blob_name)
