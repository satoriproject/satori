#!/usr/bin/env python2
# -*- coding: utf-8 -*-
# vim:ts=4:sts=4:sw=4:expandtab

from satori.client.common import want_import
from satori.tools import options, setup, authenticate

from satori.kolejka.util import ctxyaml, blob
from satori.kolejka.util.upload import upload_blob

import json
import os.path

options.add_argument('--status', dest='status', type=str)
options.add_argument('--description', dest='description', type=str)
options.add_argument('--results', dest='results', type=str)
options.add_argument('--kolejka-result', dest='kolejka', type=str)
options.add_argument('id', type=int)

def main():
    want_import(globals(), '*')
    option_values = setup()
    authenticate()
    result = dict()
    if option_values.kolejka:
        try:
            with open(option_values.kolejka, 'rb') as kolejka_json_file:
                y = json.load(kolejka_json_file)
        except:
            result['status'] = {'is_blob':False, 'value':'INT'}
            result['description'] = {'is_blob':False, 'value':'Checking result is heavily malformed.'}
        kolejka_ok = True
        try:
            if int(y.get('result')) != 0:
                kolejka_ok = False
        except:
            pass
        if not kolejka_ok:
            kolejka_stderr = 'Unknown error'
            kolejka_json_dir = os.path.dirname(os.path.realpath(option_values.kolejka))
            kolejka_stderr_path = os.path.join(kolejka_json_dir, 'console_stderr.txt')
            try:
                with open(kolejka_stderr_path, 'r') as kolejka_stderr_file:
                    kolejka_stderr = kolejka_stderr_file.read()
            except:
                pass
            result['status'] = {'is_blob':False, 'value':'INT'}
            result['description'] = {'is_blob':False, 'value':'Checking failed: '+kolejka_stderr}

    if option_values.results:
        with open(option_values.results, 'rb') as results_yaml_file:
            y = ctxyaml.load(results_yaml_file)
        for k,v in y.items():
            if isinstance(v, dict) and 'satori' in v:
                for key, val in v['satori'].items():
                    if isinstance(val, blob.Blob):
                        result[key] = upload_blob(val.path)
                    else:
                        result[key] = AnonymousAttribute(is_blob=False, value=val)
    if option_values.status:
        result['status'] = {'is_blob':False, 'value': option_values.status}
    if option_values.description:
        result['description'] = {'is_blob':False, 'value': option_values.description}
    if 'status' not in result:
        result['status'] = {'is_blob':False, 'value':'INT'}
        result['description'] = {'is_blob':False, 'value':'Checking did not create a proper result.'}
    Judge.set_result(option_values.id, result)

if __name__ == '__main__':
    main()
