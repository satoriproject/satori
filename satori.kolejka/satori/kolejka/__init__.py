# -*- coding: utf-8 -*-
# vim:ts=4:sts=4:sw=4:expandtab

import os.path

option_values = None

import satori.tools
from satori.tools import options, authenticate

DEFAULT_WORK_DIR = 'satori.kolejka.work_dir'

options.add_argument('--work-directory', dest='work_dir', default=DEFAULT_WORK_DIR, type=str)

def conf_name(test_result):
    return 'S'+str(test_result)
def conf_tests_path(test_result):
    return os.path.join( option_values.work_dir, 'tests', conf_name(test_result) )
def conf_satoriid_path(test_result):
    return os.path.join( conf_tests_path(test_result), 'satori.id' )
def conf_task_path(test_result):
    return os.path.join( conf_tests_path(test_result), 'task' )
def conf_kolejkaid_path(test_result):
    return os.path.join( conf_tests_path(test_result), 'kolejka.id' )
def conf_result_path(test_result):
    return os.path.join( conf_tests_path(test_result), 'result' )
def conf_done_path(test_result):
    return os.path.join( option_values.work_dir, 'done', conf_name(test_result) )

def setup():
    global option_values
    option_values = satori.tools.setup()

def option_value(name):
    global option_values    
    return option_values.__getattribute__(name)
