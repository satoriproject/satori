#!/bin/bash

DEST=/usr/local/satori
BIN=/usr/local/bin

OFFICE="$(dirname "$(readlink -f "$(which "$0")")")"
pushd "$OFFICE"
unset PYTHONPATH

apt-get -y install python-dev libpopt-dev libcurl4-openssl-dev libpq-dev libyaml-dev libcap-dev make patch
apt-get -y install virtualenv || apt-get -y install python-virtualenv

VENV_BIN=virtualenv2
if ! which "$VENV_BIN" >& /dev/null; then
	VENV_BIN=virtualenv
fi

rm -Rf "$DEST" && mkdir "$DEST" && pushd "$DEST" || exit 1
"${VENV_BIN}" --python=python2 --prompt=satori . &&
"${VENV_BIN}" --python=python2 --prompt="(satori) " . &&
source bin/activate &&
pip install "pip<10"
popd

find satori.objects/eggs satori.ars/eggs satori.client.common/eggs satori.tools/eggs -mindepth 1 -maxdepth 1 -iname "*.egg" -exec easy_install {} \;

for i in satori.objects satori.ars satori.client.common satori.tools ; do
    pushd "$i" || exit 1; python setup.py install; popd
done



for tool in console problems; do
cat > "${BIN}/satori.${tool}" <<EOF
#!/bin/bash

source "${DEST}/bin/activate"

exec "${DEST}/bin/satori.${tool}" "\$@"
EOF
chmod a+x "${BIN}/satori.${tool}" || exit 1
done

popd
