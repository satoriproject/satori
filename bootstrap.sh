#!/bin/bash
OFFICE="$(dirname "$(readlink -f "$(which "$0")")")"
VENV=".virtual_env"
pushd "$OFFICE"
unset PYTHONPATH

apt-get -y install python-dev libpopt-dev libcurl4-openssl-dev libpq-dev libyaml-dev libcap-dev make patch
apt-get -y install virtualenv || apt-get -y install python-virtualenv

VENV_BIN=virtualenv2
if ! which "$VENV_BIN" >& /dev/null; then
	VENV_BIN=virtualenv
fi

"${VENV_BIN}" --python=python2 --prompt="(satori) " "$VENV" &&
mkdir -p bin &&
find "$VENV/bin" -name "activate*" |while read a; do ln -s "../$a" bin; done &&
source bin/activate &&
pip install "pip<10"

popd
