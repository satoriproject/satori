Satori Testing System
=====================

Satori is an abandoned project and is not safe for use.
It is written in a deprecated language (Python 2) and depends on deprecated versions of many libraries.

Satori is a free testing system which allows to run programming contests (ICPC style) and programming courses.

Satori was originally developed by [Theoretical Computer Science, Jagiellonian University](http://tcs.uj.edu.pl/)
Satori development was supported by the European Social Fund in Poland.

Satori is distributed under the terms of the MIT License.

Satori source code repository, documentation, and issue tracking is hosted on [Bitbucket](https://bitbucket.org/satoriproject/satori/)
