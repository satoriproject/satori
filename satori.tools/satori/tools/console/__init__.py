# vim:ts=4:sts=4:sw=4:expandtab

import logging

def main():
    from satori.tools import options, setup

    flags = setup(logging.INFO)

    import code
    console = code.InteractiveConsole()
    console.runcode('from satori.client.common import want_import')
    console.runcode('want_import(globals(), "*")')
    console.interact()
